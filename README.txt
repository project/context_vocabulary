Context Vocabulary
==================

This module works with Drupal 7.x and Context 3.x

Context Vocabulary provides a context condition reactive to current 
node taxonomy.
Core context taxonomy condition is per-term basis. 
This module provides a per-vocabulary condition, means if at least 
one term of vocabulary A is selected, the context is active.


Installation
------------

1. Put the module in 'sites/all/modules'

2. Enable the module on the 'admin/build/modules' page.


Maintainers
-----------

- opi (Olivier PIERRE) (6.x)
- zhuber (Zak Huber) (7.x)
