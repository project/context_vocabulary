<?php

/**
 * @file
 * Module file for the context_vocabulary module, which creates a vocabulary condition.
 */

function context_keywords_ctools_plugin_api($module, $api) {
  if ($module == 'context' && $api == 'plugins') {
    return array('version' => 3);
  }
}

/**
 * Implements hook_context_plugins().
 */
function context_vocabulary_context_plugins() {
  $plugins = array();
  $plugins['context_vocabulary_context_condition_vid'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_vocabulary') . '/plugins',
      'file' => 'context_vocabulary_context_condition_vid.inc',
      'class' => 'context_vocabulary_context_condition_vid',
      'parent' => 'context_condition',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function context_vocabulary_context_registry() {
  return array(
    'conditions' => array(
      'vid' => array(
        'title' => t('Vocabulary'),
        'plugin' => 'context_vocabulary_context_condition_vid',
      ),
    ),
  );
}

/**
* Implements hook_node_view().
*/
function context_vocabulary_node_view($node, $view_mode, $langcode) {
  if ($view_mode == 'full' && $plugin = context_get_plugin('condition', 'vid')) {
    $plugin->execute($node, 'view');
  }
}

/**
* Implements hook_ctools_render_alter().
*/
function context_vocabulary_ctools_render_alter(&$info, &$page, &$context) {
  // This function handles panels/panelizer nodes, which do not go through hook_node_view.
  if ($context['handler']->task == 'node_view' && $plugin = context_get_plugin('condition', 'vid')) {
    $node = node_load($context['args'][0]);
    $plugin->execute($node, 'view');
  }
}

/**
 * Implements hook_form_alter().
 */
function context_vocabulary_form_alter(&$form, $form_state, $form_id) {
  // Prevent this from firing on admin pages... damn form driven apis...
  if ($form['#id'] === 'node-form' && arg(0) != 'admin') {
    if ($plugin = context_get_plugin('condition', 'vid')) {
      $plugin->execute($form['#node'], 'form');
    }
  }
}
